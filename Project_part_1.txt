﻿Ryan Thong
Part 1
Analyzing Diboson(WW):


Identify the final state of your signal:
Leptons: 1
Jets: 4
Neutrinos: yes, 1


Standard Model Processes with same final state:
1. tt-bar
2. ZZ
3. WZ
4. Z
5. W


Samples from the list of datasets to use and why.
1. mc_105985.WW.root: because this is the process I want to analyze
2. mc_105986.ZZ.root: because this process has a similar final state with WW
3. mc_105987.WZ.root: process has a similar final state with WW
4. Mc_117050.ttbar_lep.root: this process has a similar final state with WW
5. Z: (files associated are below) because they have a similar final sate with WW
   1. mc_147770.Zee.root
   2. mc_147771.Zmumu.root
   3. mc_147772.Ztautau.root


The above processes from the above dataset are ones I know to be what I should be analyzing and are able to be considered background. I am not sure what “b veto” means, to consider that as part of my considered dataset to analyze.